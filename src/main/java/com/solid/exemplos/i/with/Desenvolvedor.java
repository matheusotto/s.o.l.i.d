package com.solid.exemplos.i.with;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Desenvolvedor extends Funcionario implements Convencional {

    private double salario;

    @Override
    public double getSalario() {
        return this.salario + this.getSalario();
    }

    @Override
    public String toString() {
        return "Desenvolvedor [salario=" + salario + "]";
    }
}
