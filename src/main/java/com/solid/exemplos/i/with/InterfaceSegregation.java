package com.solid.exemplos.i.with;

/*
Diz que “muitas interfaces específicas são melhores do que uma interface geral”.
Esse princípio trata da coesão em interfaces, da construção de módulos enxutos, ou seja,
com poucos comportamentos. Interfaces que possuem muitos comportamentos são difíceis de
manter e evoluir, e devem ser evitadas.
Para melhor entendimento, vamos voltar ao exemplo do Funcionario e transformar essa
classe em uma classe abstrata, com outros dois métodos abstratos: getSalario() e getComissao().
* */

public class InterfaceSegregation {
}
