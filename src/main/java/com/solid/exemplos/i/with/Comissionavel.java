package com.solid.exemplos.i.with;

public interface Comissionavel {

    public double getComissao();
}
