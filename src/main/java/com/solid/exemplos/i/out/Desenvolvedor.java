package com.solid.exemplos.i.out;

public class Desenvolvedor extends Funcionario {

    private double salario;

    public Desenvolvedor(double salario) {
        this.salario = salario;
    }

    @Override
    public double getSalario() {
        return this.salario;
    }

    @Override
    // NÃO FAZ SENTIDO O GET COMISSÃO
    public double getComissao() {
        return 0d;
    }
}
