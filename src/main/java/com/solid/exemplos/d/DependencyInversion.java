package com.solid.exemplos.d;

public class DependencyInversion<TabelaDePrecoAVista, TabelaDePrecoAPrazo> {

    /*
    Diz que devemos “depender de abstrações e não de classes concretas”.
    Uncle Bob quebra a definição desse princípio em dois sub-itens:
    “Módulos de alto nível não devem depender de módulos de baixo nível.”
    “As abstrações não devem depender de detalhes. Os detalhes devem depender das abstrações.”
    E isso se dá porque abstrações mudam menos e facilitam a mudança de comportamento e as futuras evoluções do código.
    Quando falamos do OCP, também vimos um exemplo do DIP, mas não falamos exclusivamente dele. Ao fazermos a
    refatoração da classe CalculadoraDePrecos, ao invés de termos dependência direta das classes concretas
    */

    //TabelaDePrecoAVista,TabelaDePrecoAPrazo

    /*para calcular o desconto tabelado do produto e o frete, invertemos
    e passamos a depender de duas interfaces TabelaDePreco e Frete. Assim, passamos a evoluir e manter apenas
    as classes concretas, ou seja, os detalhes.
     */

}
