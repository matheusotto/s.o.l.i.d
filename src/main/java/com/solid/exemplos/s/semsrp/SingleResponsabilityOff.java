package com.solid.exemplos.s.semsrp;

import java.sql.Connection;

public class SingleResponsabilityOff {
    static class Funcionario {
        private Integer id;
        private String nome;
        private Double salario;
        private Connection connection;

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public Double getSalario() {
            return salario;
        }

        public void setSalario(Double salario) {
            this.salario = salario;
        }

        public Double calculaSalario() {
            return this.salario - (this.salario * 0.225);
        }
    }
}
