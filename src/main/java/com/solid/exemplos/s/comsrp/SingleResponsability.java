package com.solid.exemplos.s.comsrp;

/*
1. Princípio da Responsabilidade Única (SRP)

A class should have one, and only one, reason to change.
Esse primeiro princípio diz que “uma classe deve ter apenas um motivo para mudar”,
ou seja, deve ter uma única responsabilidade. Basicamente, esse princípio trata especificamente a coesão.
A coesão é definida como a afinidade funcional dos elementos de um módulo. Se refere ao relacionamento que os
membros desse módulo possuem, se possuem uma relação mais direta e importante. Dessa forma, quanto mais bem
definido o que sua classe faz, mais coesa ela é.
Veja o exemplo da classe Funcionario. Ela que contém dois métodos: calculaSalario(),
que calcula o salário do funcionário, com desconto do imposto; e o salva(), que é o método de controle
de persistência, ou seja, abre uma conexão com o banco de dados e salva o funcionário no banco.
* */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

public class SingleResponsability {

    // FormaCorreta SRP

    @Data
    static class ConnectionDAO{
        private Properties connectionProps;
        private Connection conn;
        private String dbms;
        private String dbName;
        private String serverName;
        private String portNumber;

        private static final String JDBC = "jdbc:";

        public ConnectionDAO (String username, String password){
            super();
            connectionProps = new Properties();
            connectionProps.put("user", username);
            connectionProps.put("password", password);
        }
    }

    static class FuncionarioDAO {

        private static final Logger logger = Logger.getLogger(String.valueOf(FuncionarioDAO.class));

        public void salva(Funcionario funcionario) throws SQLException{

            ConnectionDAO connectionDAO = new ConnectionDAO("root", "");
        }

    }

    static interface RegraDeCalculo {
        public double calcula (Funcionario funcionario);
    }

    enum Cargo {
        DESENVOLVEDOR_SENIOR(new RegraVinteDoisEMeioPorcento()),
        DESENVOLVEDOR_JUNIOR(new RegraOnzePorcento());

        private RegraDeCalculo regra;

        Cargo(RegraDeCalculo regra){
            this.regra = regra;
        }

        public RegraDeCalculo getRegra() {
            return regra;
        }

    }

    static class RegraVinteDoisEMeioPorcento implements RegraDeCalculo {

        @Override
        public double calcula(Funcionario funcionario) {
            return funcionario.getSalario() - (funcionario.getSalario() * 0.225);
        }
    }

    static class RegraOnzePorcento implements RegraDeCalculo {

        @Override
        public double calcula(Funcionario funcionario) {
            return funcionario.getSalario() - (funcionario.getSalario() * 0.11);
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class Funcionario {
        private Integer id;
        private String nome;
        private double salario;
        private Cargo cargo;

        @Override
        public String toString() {
            return "Funcionario [id=" + id + ", nome=" + nome + ", salario=" + salario + ", cargo=" + cargo + "]";
        }
    }
}


