package com.solid.exemplos.o.ocpoff;

import lombok.Getter;

@Getter
class Produto {

    public int valor;
    public String estado;

    public int getMeioPagamento() {
        return 0;
    }
}

class Frete {

    public double calculaFrete(String estado) {

        if ("SAO PAULO".equals(estado.toUpperCase())) {
            return 7.5;
        } else if ("MINAS GERAIS".equals(estado.toUpperCase())) {
            return 12.5;
        } else if ("RIO DE JANEIRO".equals(estado.toUpperCase())) {
            return 10.5;
        } else {
            return 10.0;
        }
    }
}

class TabelaDePrecoAVista {

    public double calculaDesconto(double valor) {
        if(valor > 100.0) {
            return 0.05;
        }else if(valor > 500.0) {
            return 0.07;
        }else if(valor > 1000.0) {
            return 0.10;
        }else {
            return 0d;
        }
    }
}

class TabelaDePrecoAPrazo {

    public double calculaDesconto(double valor) {
        if(valor > 100.0) {
            return 0.01;
        }else if(valor > 500.0) {
            return 0.02;
        }else if(valor > 1000.0) {
            return 0.05;
        }else {
            return 0d;
        }
    }

}

public class OpenClosed {

    public double calcula(Produto produto) {

        Frete frete = new Frete();
        double desconto = 0d;

        int regra = produto.getMeioPagamento();

        switch (regra) {
            case 1:
                System.out.println("Venda à vista");
                TabelaDePrecoAVista tabela1 = new TabelaDePrecoAVista();
                desconto = tabela1.calculaDesconto(produto.getValor());
                break;
            case 2:
                System.out.println("Venda à prazo");
                TabelaDePrecoAPrazo tabela2 = new TabelaDePrecoAPrazo();
                desconto = tabela2.calculaDesconto(produto.getValor());
                break;
        }

        double valorFrete = frete.calculaFrete(produto.getEstado());
        return produto.getValor() * (1 - desconto) + valorFrete;
    }

}
