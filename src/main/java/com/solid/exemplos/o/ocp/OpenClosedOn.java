package com.solid.exemplos.o.ocp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
Diz que “as entidades de software (classes, módulos, funções etc.)
devem ser abertas para ampliação, mas fechadas para modificação”. De forma mais detalhada,
diz que podemos estender o comportamento de uma classe, quando for necessário, por meio de
herança, interface e composição, mas não podemos permitir a abertura dessa classe para fazer
pequenas modificações.
Para ilustrar o entendimento desse princípio, vamos observar a classe CalculadoraDePrecos, que
pertence à um sistema de e-commerce fictício.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
class Produto {

    private int valor;
    private String estado;

}

interface TabelaDePreco {

    public double calculaDesconto(double valor);

}

interface ServicoDeFrete {

    public double calculaFrete(String estado);

}

class TabelaDePrecoAPrazo implements TabelaDePreco {


    @Override
    public double calculaDesconto(double valor) {
        if (valor > 100.0){
            return 0.01;
        } else if (valor > 500.0) {
            return 0.02;
        } else if (valor > 1000.0) {
            return 0.05;
        } else {
            return 0d;
        }
    }
}

class TabelaPrecoAVista implements TabelaDePreco {

    @Override
    public double calculaDesconto(double valor) {
        if (valor > 100.0){
            return 0.05;
        } else if (valor > 500.0) {
            return 0.07;
        } else if (valor > 1000.0) {
            return 0.10;
        } else {
            return 0d;
        }
    }
}

class Frete implements ServicoDeFrete {

    @Override
    public double calculaFrete(String estado) {
        switch (estado.toUpperCase()) {
            case "SAO PAULO":
                return 7.5;
            case "MINAS GERAIS":
                return 12.5;
            case "RIO DE JANEIRO":
                return 10.5;
            default:
                return 10.0;
        }
    }
}

class CalculadoraDePrecos {

    private TabelaDePreco tabela;
    private ServicoDeFrete frete;

    public CalculadoraDePrecos(TabelaDePreco tabela, ServicoDeFrete frete) {
        this.tabela = tabela;
        this.frete = frete;
    }

    public double calcula(Produto produto) {
        double desconto = tabela.calculaDesconto(produto.getValor());
        double valorFrete = frete.calculaFrete(produto.getEstado());
        return produto.getValor() * (1 - desconto) + valorFrete;
    }

}
