package com.solid.exemplos.l.lspoff;


///-- --- Esse método faria o banco entrar em LOOP

public class ContaSalario extends ContaCorrenteComum {

    public void rende() throws Exception {
        throw new Exception("Essa conta não possui rendimento");
    }

}
