package com.solid.exemplos.l.liskovsubstitution;

/*
Diz que “Os subtipos devem ser substituíveis pelos seus tipos base”,
e que as classes/tipos base podem ser substituídas por qualquer uma das suas subclasses,
ponderando sobre os cuidados para usar a herança no seu projeto de software. Mesmo a
herança sendo um mecanismo poderoso, ela deve ser utilizada de forma contextualizada e
moderada, evitando os casos de classes serem estendidas apenas por possuírem algo em comum.
Esse princípio foi descrito pela pesquisadora Barbara Liskov, em seu artigo de 1988, em que
ela explica que, antes de optar pela herança, precisamos pensar nas pré-condições e pós-condições da sua classe.
Para ficar mais claro, vamos analisar o exemplo das classes ContaCorrenteComum e ContaSalario.
A ContaCorrenteComum representa, dentro do nosso contexto simplificado, uma conta de banco qualquer.
Tem os métodos deposita(), getSaldo(), saca() e rende().
*/


import java.util.ArrayList;
import java.util.List;

public class Banco {

    public static void main(String[] args) {

        List<ContaCorrenteComum> listaDeContas = new ArrayList<>();
        listaDeContas.add(new ContaCorrenteComum());
        listaDeContas.add(new ContaSalario());

        for (ContaCorrenteComum conta : listaDeContas) {
            conta.rende();

            System.out.println("Novo Saldo:");
            System.out.println(conta.getSaldo());
        }
    }


}
